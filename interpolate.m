function w = interpolate(wi, wi_1, wi_2)
    %vi = conservativeToPrimitive(wi);
    %vi_1 = conservativeToPrimitive(wi_1);
    %vi_2 = conservativeToPrimitive(wi_2);
    %v = interpolateSecondOrderUpwind(vi, vi_1, vi_2);
    %w = primitiveToConservative(v);
    w = wi_1;
end




function f = interpolateSecondOrderUpwind(fi, fi_1, fi_2)
% 	
% 	|------------|------------|-------------|
% 	|            |            |             |
% 	|            |            |             |
% 	|    fi_2    |   fi_1    *|     f_i     |
% 	|            |            |             |
% 	|            |            |             |
% 	|------------|------------|-------------|
%     Interpolated value at the face denoted by *
% 	
    r = (fi - fi_1)./(fi_1 - fi_2 +1e-12);
	r = max(r,0.0);
	phi = 2*r./(1+r);%van leer's limiter
	f = fi_1 + 0.5*phi.*(fi_1 - fi_2);
end