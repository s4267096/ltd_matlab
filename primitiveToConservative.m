function w = primitiveToConservative(v)
    gmin1 = 0.4;
    w = [v(1);
        v(1)*v(2);
        v(1)*v(3);
        v(4)/gmin1 + 0.5*v(1)*(v(2)*v(2) + v(3)*v(3))];
end