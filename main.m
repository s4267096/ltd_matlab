function v = main(n,t,cfl)
    x_hi = 1;
    y_hi = 1;
    xs = linspace(0,x_hi,n);
    ys = linspace(0,y_hi,n);
    [X,Y] = meshgrid(xs,ys);
    dx = x_hi/(n-1); dy = y_hi/(n-1);
    vol = dx*dy;
    %% 
    % each edge requires a wL and wR
    w = zeros(4,n+4,n+4);
    vols = ones(n+4,n+4)*vol;
    %% boundary conditions
    T = 300; rho = 1.2; M = 2; g = 1.4; R = 287; cv = R/(g-1); cp = g*cv;
    alpha = deg2rad(2); a = sqrt(g*R*T); v_inf = M*a; u = v_inf*cos(alpha); v = -v_inf*sin(alpha);
    W_inlet = [rho; rho*u; rho*v; rho*(cv*T + 0.5*v_inf*v_inf)];
    dt = cfl*dx/a
    w = repmat(W_inlet,1,n+4,n+4);
    maxits = ceil(t/dt)
    for nits = 1:maxits
        w_derivs = zeros(4,n+4,n+4);
        % reflect for bottom boundary
        w(:,3:n+2,1) = w(:,3:n+2,5);
        w(3,3:n+2,1) = -w(3,3:n+2,5);
        w(:,3:n+2,2) = w(:,3:n+2,4);
        w(3,3:n+2,2) = -w(3,3:n+2,4);
        % set vertical velocity to zero??
        w(3,3:n+2,3) = 0;
        %% reconstruct, compute fluxes and add to derivative
        for i = 2:n+2
            for j = 2:n+2
                if j > 2 && j < n+3
                    if i == 2
                        bcFlag = 'L';
                    elseif i == n+2
                        bcFlag = 'R';
                    else
                        bcFlag = 'N';
                    end
                    flux_x = roe(interpolate(w(:,i+1,j), w(:,i,j), w(:,i-1,j)), interpolate(w(:,i,j),w(:,i+1,j),w(:,i+2,j)),g,'X',bcFlag);
                    if i ~= 2 % left boundary
                        w_derivs(:,i,j) = w_derivs(:,i,j) - flux_x/vols(i,j);
                    end
                    if i ~= n+2 %right boundary
                        w_derivs(:,i+1,j) = w_derivs(:,i+1,j) + flux_x/vols(i+1,j);
                    end
                end
                if i > 2 && i < n+3
                    if j == n+2
                        bcFlag = 'R';
                    else
                        bcFlag = 'N';
                    end
                    flux_y = roe(interpolate(w(:,i,j+1),w(:,i,j),w(:,i,j-1)), interpolate(w(:,i,j), w(:,i,j+1),w(:,i,j+1)),g,'Y',bcFlag);
                    if j ~= 2 % bottom boundary
                        w_derivs(:,i,j) = w_derivs(:,i,j) - flux_y/vols(i,j);
                    end
                    if j ~= n+2 %top boundary
                        w_derivs(:,i,j+1) = w_derivs(:,i,j+1) + flux_y/vols(i,j+1);
                    end
                end
            end
        end
        % update cells
        w = w + w_derivs*dt;
        fprintf('n: %d \n',nits);
    end
    v = zeros(4,n+4,n+4);
    for i = 1:n+4
        for j = 1:n+4
            v(:,i,j) = conservativeToPrimitive(w(:,i,j));
        end
    end
    p = squeeze(v(4,:,:));
    rho = squeeze(v(1,:,:));
    uvel = squeeze(v(2,:,:));
    vvel = squeeze(v(3,:,:));
    surf(p)
end
