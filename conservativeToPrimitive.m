function v = conservativeToPrimitive(w)
    gmin1 = 0.4;
    v = [w(1);
        w(2)/w(1);
        w(3)/w(1);
        (w(4) - 0.5*(w(2)^2 + w(3)^2)/w(1))*gmin1];
end