function flux = roe(wL, wR, g, orient,BC)
    gmin1 = g - 1;
    sq_rho_R = sqrt(wR(1));
    sq_rho_L = sqrt(wL(1));
    deltaW = wR - wL;
    if orient == 'X'
        uL = wL(2)/wL(1); uR = wR(2)/wR(1);
        vL = wL(3)/wL(1); vR = wR(3)/wR(1);
        deltaW = deltaW([1,2,4]);
    elseif orient == 'Y'
        uL = wL(3)/wL(1); uR = wR(3)/wR(1);
        vL = wL(2)/wL(1); vR = wR(2)/wR(1);
        deltaW = deltaW([1,3,4]);
    end
    velmagL = uL*uL + vL*vL; velmagR = uR*uR + vR*vR;
    hL = g*wL(4)/wL(1) - 0.5*gmin1*velmagL; hR = g*wR(4)/wR(1) - 0.5*gmin1*velmagR;
    pL = (wL(4) - 0.5*wL(1)*velmagL)*gmin1; pR = (wR(4) - 0.5*wR(1)*velmagR)*gmin1;
    u_RL = (sq_rho_R*uR + sq_rho_L*uL)/(sq_rho_R + sq_rho_L);
    h_RL = (sq_rho_R*hR + sq_rho_L*hL)/(sq_rho_R + sq_rho_L);
    c_RL = sqrt(gmin1*(h_RL - 0.5*u_RL*u_RL));
    M_RL = u_RL/c_RL;
    if BC == 'L'
        cR = sqrt(g*pR/wR(1));
        M = uR/cR;
    elseif BC == 'R'
        cL = sqrt(g*pL/wL(1));
        M = uL/cL;
    else
        M = M_RL;
    end
    %quick method of computing fluxes
    if M > 1.0
        flux3 = [wL(1)*uL; 
           wL(1) * uL * uL + pL;
           (wL(4) + pL) * uL];
    elseif M > 0.0
        Flux_L = [wL(1)*uL; 
           wL(1) * uL * uL + pL;
           (wL(4) + pL) * uL];
        eig3 = u_RL - c_RL;
        l3 = [(gmin1)*M*M/4 + 0.5*M;
            (-0.5 - 0.5*M*(gmin1))/c_RL;
            gmin1/2.0/c_RL/c_RL];
        r3 = [1;
            u_RL - c_RL;
            c_RL*c_RL/gmin1 - u_RL*(2*c_RL - u_RL)/2.0];
        flux3 = Flux_L + eig3*dot(l3, deltaW)*r3;
    elseif M > -1.0
        Flux_R = [wR(1)*uR; 
            wR(1) * uR * uR + pR;
            (wR(4) + pR) * uR];
        eig1 = u_RL + c_RL;
        M = u_RL/c_RL;
        l1 = [(gmin1)*M*M/4 - 0.5*M;
            (0.5 - 0.5*M*(gmin1))/c_RL;
            gmin1/2.0/c_RL/c_RL];
        r1 = [1;
            u_RL + c_RL;
            c_RL*c_RL/gmin1 + u_RL*(2*c_RL + u_RL)/2.0];
        flux3 = Flux_R - eig1*dot(l1, deltaW)*r1;
    else
        flux3 = [wR(1)*uR; 
            wR(1) * uR * uR + pR;
            (wR(4) + pR) * uR];
    end
    if flux3(1) > 0
        u = uL; v = vL;
    else
        u = uR; v = vR;
    end
    if orient == 'X'
        flux = [flux3(1);
            flux3(2);
            flux3(1)*v;
            flux3(3)];
    elseif orient == 'Y'
        flux = [flux3(1);
            flux3(1)*u;
            flux3(2);
            flux3(3)];
    end
end

